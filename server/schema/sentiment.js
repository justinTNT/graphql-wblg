const mongoose = require("mongoose");

const {Schema} = mongoose;
const {ObjectId} = Schema;

const name = "Sentiment";
const schema = new Schema({
  subject: ObjectId,
  guest: ObjectId,
  sentiment: Number,
});

module.exports = { name, schema };


