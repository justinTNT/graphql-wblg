const mongoose = require("mongoose");

const {Schema} = mongoose;

const name = "Tag";
const schema = new Schema({
  name: String
});

module.exports = { name, schema };

