const Schema = (db) => {
  let { name, schema } = require('./tag');
  const Tag = db.model(name, schema);

  ({ name, schema } = require('./guest'));
  const Guest = db.model(name, schema);

  ({ name, schema } = require('./story'));
  const Story = db.model(name, schema);

  ({ name, schema } = require('./comment'));
  const Comment = db.model(name, schema);

  ({ name, schema } = require('./sentiment'));
  const Sentiment = db.model(name, schema);

  return { Sentiment, Tag, Guest, Story, Comment };
}

module.exports = Schema;
