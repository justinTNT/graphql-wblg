const mongoose = require("mongoose");

const {Schema} = mongoose;
const {ObjectId} = Schema;

const name = "Story";
const schema = new Schema({
  name: String,
  url: String,
  title: String,
  image: String,
  teaser: String,
  comment: String,
  body: String,
  tags: [ObjectId],
  comments: [ObjectId],
  created_date: Date,
  modified_date: Date
});

module.exports = { name, schema };

