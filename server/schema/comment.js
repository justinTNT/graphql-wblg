const mongoose = require("mongoose");

const {Schema} = mongoose;
const {ObjectId} = Schema;

const name = "Comment";
const schema = new Schema({
  comment: String,
  guest: ObjectId,
  subject: ObjectId,
  parent: ObjectId,
  created_date: Date
});

module.exports = { name, schema };

