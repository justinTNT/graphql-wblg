const mongoose = require("mongoose");

const {Schema} = mongoose;

const name = "Guest";
const schema = new Schema({
  handle: String,
  password: String,
  email: String,
  verifiedEmail: String, // once email is confirmed, it is copied to this field
  name: String,
  url: String,
  role: String,
  created_date: Date,
});

module.exports = { name, schema };

