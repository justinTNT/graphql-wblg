const Koa = require('koa');
const mongoose = require('mongoose');

mongoose.set('useFindAndModify', false);

const { ApolloServer } = require('apollo-server-koa');

const config = require('./config');
const middleWare = require('./middleware');

const db = mongoose.createConnection(config.MONGO_URL);
const resolvers = require('./graph/resolvers/')(db);

const typeDefs = require('./graph/typedefs');

const context = ({ ctx }) => {
  return { ctx };
};

const server = new ApolloServer({ typeDefs, resolvers, context });
const app = new Koa();

app.use(middleWare({ app }));
server.applyMiddleware({ app });
const port = config.GRAPHQL_PORT;
app.listen({ port });
