const compose = require('koa-compose');
const convert = require('koa-convert');
const cors = require('koa-cors');
const bodyParser = require('koa-bodyparser');
const session = require('koa-session');
const cookie = require('koa-cookie').default;

module.exports = function middleware({ app }) {
  // eslint-disable-next-line no-param-reassign
  app.keys = ['sooper-seecret-key'];

  return compose([
    convert(cors({ credentials: true })),
    convert(bodyParser()),
    convert(cookie()),
    convert(session(app))
  ]);
};
