const crypto = require('crypto');

const passwordMatchesHash = (password, hash) => {
  const salt = hash.slice(0, 16);
  const pass = hash.slice(16);
  const calc = crypto
    .createHmac('sha512', salt)
    .update(password)
    .digest('hex');
  return pass === calc;
};

const hashFromPassword = password => {
  const salt = crypto
    .randomBytes(8)
    .toString('hex')
    .slice(0, 16);
  const calc = crypto
    .createHmac('sha512', salt)
    .update(password)
    .digest('hex');
  return salt + calc;
};
module.exports = { passwordMatchesHash, hashFromPassword };
