const { gql } = require('apollo-server-koa');

const typeDefs = gql`
  type Query {
    stories(skip: Int, first: Int, tag: String): [Story]
    story(name: String!): Story
    tags: [Tag]
    tag(id: ID!): Tag
    getCurrentUser: Guest
  }

  type Mutation {
    updateProfile(img: String, email: String!, name: String!): Profile
    addComment(comment: String, subject: ID, parent: ID): Comment
    addSentiment(sentiment: Int, subject: ID): Boolean
    facebook(user: String!, url: String, email: String): Guest
    register(username: String!, password: String!, email: String!): Guest
    login(username: String!, password: String!): Guest
    logout: Boolean
  }

  type mongoID {
    oid: ID
  }

  type mongoDate {
    date: String
  }

  type Story {
    _id: mongoID!
    id: ID!
    name: String!
    comment: String!
    image: String!
    teaser: String!
    title: String!
    url: String!
    modified_date: mongoDate
    created_date: mongoDate
    created: String
    tags: [Tag]
    sentiment: Int!
    likes: Int!
    dislikes: Int!
    comments: [Comment]
  }

  type Tag {
    _id: mongoID!
    id: ID!
    name: String!
  }

  type Comment {
    _id: mongoID!
    id: ID!
    author: Guest!
    guest: mongoID!
    created_date: mongoDate!
    subject: String!
    parent: String!
    comment: String!
    sentiment: Int!
    likes: Int!
    dislikes: Int!
  }

  type Guest {
    _id: mongoID
    id: ID!
    handle: String!
    email: String!
    confirmed: Boolean!
    name: String!
    url: String!
    role: String!
  }

  type Sentiment {
    _id: mongoID!
    id: ID!
    guest: mongoID!
    subject: mongoID!
    sentiment: Int
  }

  type Profile {
    url: String
    email: String
    name: String
  }
`;

module.exports = typeDefs;
