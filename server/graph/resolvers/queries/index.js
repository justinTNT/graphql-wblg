const ld = require('lodash');

const sessionQueries = require('./session');
const storyQueries = require('./story');
const tagQueries = require('./tag');

module.exports = ({ Guest, Tag, Story }) => ld.extend(
  {}, 
  sessionQueries( { Guest } ),
  storyQueries( { Story, Tag } ),
  tagQueries( { Tag } ),
);

