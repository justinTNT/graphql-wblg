const ld = require('lodash');

const sessionQueries = ( { Guest } ) => {
  return {
    getCurrentUser: async (_, __, {ctx}) =>
      Guest.findOne( { _id: ld.get(ctx.session, 'user.id') } ),
  };
};

module.exports = sessionQueries;

