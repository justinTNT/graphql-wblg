const resolvers = ({ Tag }) => {
  return {
    tag: async (_, args) => Tag.findOne(args),

    tags: async () => Tag.find(),
  };

};

module.exports = resolvers;

