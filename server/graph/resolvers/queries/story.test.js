const ld = require('lodash');

const storyResolvers = require('./story');


const storyFixtures = [
  {
    id: '101',
    name: 'lead',
    tags: [2],
  }, {
    id: '102',
    name: 'follow-up',
    tags: [],
  }
];

const tagFixtures = [
  {
    _id: 1,
    id: '1',
    name: 'popular',
  }, {
    _id: 2,
    id: '2',
    name: 'feature',
  }
];

const Tag = {
  findOne: (args) => ld.find(tagFixtures, args),
};

const Story = {
  findOne: (args) => {
    return ld.find(storyFixtures, args);
  },
  find: (args) => {
    return {
      skip: () => {
        return {
          limit: () => {
            return {
              exec: () => {
                if (args.tags) {
                  return ld.filter(storyFixtures, (story) => ld.intersection(story.tags, args.tags.$in).length);
                } 
                  return storyFixtures;
                
              }
            }
          }
        }
      }
    }
  },
};

const { story, stories } = storyResolvers({ Story, Tag });

const parent = {};
const ctx = {};

describe('Story Resolvers', () => {
  it('story', async () => {
    const output = await story(parent, { name: 'follow-up' }, ctx);
    expect(output.id).toEqual('102');
  });

  it('stories', async () => { 
    const output = await stories(parent, {}, ctx);
    expect(output).toHaveLength(storyFixtures.length);
  });

  it('stories by tag', async () => { 
    const output = await stories(parent, { tag: 'feature' }, ctx);
    expect(output).toHaveLength(1);
  });
});


