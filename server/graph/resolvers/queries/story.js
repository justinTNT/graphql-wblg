const resolvers = ({ Story, Tag }) => {
  return {
    story: async (_, args) => Story.findOne({ name: args.name }),

    stories: async (_, args) => {
      const query = {};
      const name = args.tag;
      if (name) {
        const tag = await Tag.findOne( { name } );
        query.tags = { $in: [ tag._id ] };
      }
      return Story.find(query).skip(args.skip || 0).limit(args.first || 9).exec();
    },
  };
};

module.exports = resolvers;

