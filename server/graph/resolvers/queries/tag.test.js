const ld = require('lodash');

const tagResolvers = require('./tag');


const tagFixtures = [
  {
    id: '1',
    name: 'popular',
  }, {
    id: '2',
    name: 'feature',
  }
];

const Tag = {
  findOne: (args) => ld.find(tagFixtures, args),
  find: () => tagFixtures,
}

const { tag, tags } = tagResolvers({ Tag });

const parent = {
  id: '105',
  name: 'story name',
  type: 'story',
};

const ctx = {};

describe('Tag Resolvers', () => {
  it('tag', async () => {
    const output = await tag(parent, { id: '1' }, ctx);
    expect(output.name).toEqual('popular');
  });

  it('tags', async () => { 
    const output = await tags(parent, {}, ctx);
    expect(output).toHaveLength(tagFixtures.length);
  });
});

