const ld = require('lodash');

const queries   = require("./queries");
const mutations = require("./mutations");
const types     = require("./types");

const Schema = require('../../schema/');

const resolvers = db => {
  const { Sentiment, Comment, Guest, Story, Tag } = Schema(db);

  return ld.extend(
    {},
    types({ Sentiment, Comment, Guest, Story, Tag }),
    { Query: queries({ Guest, Story, Tag }) },
    { Mutation: mutations({ Sentiment, Comment, Guest }) }
  );
};

module.exports = resolvers;
