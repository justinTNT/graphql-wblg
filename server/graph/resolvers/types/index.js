const ld = require('lodash');

const config = require('../../../config');

const mapMongoIdFrom = field => parent => parent[field].toString();
const mapMongoId = mapMongoIdFrom('_id');

const resolvers = ({ Sentiment, Comment, Guest, Tag }) => {
  const sentiment = async (parent, _, context) => {
    const { user } = context.ctx.session;
    if (user) {
      const s = await Sentiment.findOne({
        subject: parent._id,
        guest: user.id
      });
      if (s) {
        return s.sentiment;
      }
    }
    return 0;
  };

  const likes = async parent =>
    Sentiment.count({ subject: parent._id, sentiment: 1 });

  const dislikes = async parent =>
    Sentiment.count({ subject: parent._id, sentiment: -1 });

  return {
    Story: {
      id: mapMongoId,

      tags: async parent =>
        Tag.find({ _id: { $in: ld.map(parent.tags, '_id') } }),

      comments: async parent => Comment.find({ subject: parent._id }),

      sentiment,
      likes,
      dislikes
    },

    Tag: {
      id: mapMongoId
    },

    Guest: {
      id: mapMongoId,
      confirmed: parent => !!parent.verifiedEmail.length,
      email: parent => parent.email || parent.verifiedEmail,
      name: parent => parent.name || parent.handle,
      url: parent => parent.url || config.DEFAULT_AVATAR
    },

    Comment: {
      id: mapMongoId,
      author: async parent => Guest.findOne({ _id: parent.guest }),
      parent: mapMongoIdFrom('parent'),
      sentiment,
      likes,
      dislikes
    }
  };
};

module.exports = resolvers;
