const sessionResolvers = require('./session');
const sessionQueryResolver = require('../queries/session');

jest.mock('koa-passport');

const Guest = {
  findOne: () => '3'
};

const { login, logout } = sessionResolvers({ Guest }); // TODO: facebook, register
const { getCurrentUser } = sessionQueryResolver({ Guest });

describe('Session Resolvers', () => {
  it('logout', async () => {
    const ctx = { session: { user: { id: '2' } } }; // session has a user
    await logout({}, {}, { ctx });
    const user = getCurrentUser({}, {}, { ctx });
    expect(user.id).toBeUndefined();
  });

  it('login', async () => {
    // eslint-disable-next-line no-underscore-dangle
    require('koa-passport').__setUser__({ id: '3' });

    const ctx = { session: {} }; // session has no user
    const credentials = { username: 'test', password: 'test' };
    const user = await login({}, credentials, { ctx });
    expect(user).toEqual('3');
  });
});
