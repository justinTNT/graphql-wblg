const resolvers = ( { Comment } ) => {
  return {
    addComment: async (_, { subject, parent, comment }, context) => {
      const {user} = context.ctx.session;
      if (!user) { return null; }
      const newComment = new Comment({ guest: user.id, subject, parent, comment });
      return newComment.save();
    },
  };
};

module.exports = resolvers;
