const fs = require('fs-extra');
const mongoose = require('mongoose');

const { ObjectId } = mongoose.Types;

const decodeBase64Image = ascii64 => {
  const matches = ascii64.match(
    /^data:[A-Za-z-+]+\/([A-Za-z-+]+);base64,(.+)$/
  );

  if (!matches || matches.length !== 3) {
    return null;
  }

  return {
    type: matches[1],
    data: Buffer.from(matches[2], 'base64')
  };
};

const resolvers = ({ Guest }) => {
  return {
    updateProfile: async (_, { img, email, name }, context) => {
      const { user } = context.ctx.session;
      if (!user) {
        return null;
      }

      const fields = { email, name };
      if (img) {
        const decoded = decodeBase64Image(img);
        if (!decoded) {
          return null;
        }

        const fn = `${user.id}-avatar.${decoded.type}`;
        try {
          await fs.writeFile(`public/${fn}`, decoded.data);
        } catch (err) {
          return null;
        }
        fields.url = `/${fn}`;
      }

      return Guest.findOneAndUpdate({ _id: ObjectId(user.id) }, fields);
    }
  };
};

module.exports = resolvers;
