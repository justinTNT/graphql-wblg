const ld = require('lodash');

const commentMutations = require('./comment');
const sentimentMutations = require('./sentiment');
const profileMutations = require('./profile');
const sessionMutations = require('./session');

module.exports = ({ Comment, Sentiment, Guest }) =>
  ld.extend(
    {},
    commentMutations({ Comment }),
    sentimentMutations({ Sentiment }),
    profileMutations({ Guest }),
    sessionMutations({ Guest })
  );
