const mongoose = require("mongoose");

const {ObjectId} = mongoose.Types;

const resolvers = ( { Sentiment } ) => {
  return {
    addSentiment: async (_, { subject, sentiment }, context) => {
      let success = false;
      const { user } = context.ctx.session;
      if (user) {
        const query = { guest: ObjectId(user.id), subject: ObjectId(subject) }

        try {
          if (sentiment) { // upsert
            const updated = await Sentiment.findOneAndUpdate(query, { sentiment }, { upsert:true, new:true });
            success = (updated.sentiment === sentiment);
          } else { // undo
            const deleted = await Sentiment.deleteOne(query);
            success = (deleted.ok === 1);
          }
        } catch (err) {
          success = false;
        }
      }

      return success;
    },
  };
};

module.exports = resolvers;
