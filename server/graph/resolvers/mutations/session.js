const { ForbiddenError } = require('apollo-server-errors');

const { crypto } = require('../../../utils/');

const sessionQueries = ({ Guest }) => {
  return {
    facebook: async (_, args, { ctx }) => {
      const { user, url, email } = args;
      const name = user;
      const handle = user;
      ctx.session.user = null;

      let saved = null;
      let exists = await Guest.findOne({ name });
      if (exists) {
        if ((url && !exists.url) || (email && email !== exists.email)) {
          exists.email = email || exists.email;
          exists.url = exists.url || url;
          saved = await exists.save();
        }
      } else {
        const mod = new Guest({ handle, name, email, url });
        saved = await mod.save();
      }

      exists = saved || exists;
      if (exists) {
        ctx.session.user = { id: exists._id };
      }

      return exists;
    },

    register: async (_, args, { ctx }) => {
      ctx.session.user = null;
      const { email } = args;
      const name = args.username;

      const exists = await Guest.findOne({ $or: [{ name }, { email }] });
      if (exists) {
        const fieldName = exists.name === name ? 'username' : 'email';
        throw new ForbiddenError(fieldName);
      }

      const password = crypto.hashFromPassword(args.password);
      const mod = new Guest({ name, handle: name, password, email });
      const created = await mod.save();
      if (created) {
        ctx.session.user = { id: created._id };
      }
      return created;
    },

    login: async (_, args, { ctx }) => {
      ctx.session.user = null;

      const user = await Guest.findOne({ handle: args.username }).lean();
      if (user) {
        if (crypto.passwordMatchesHash(args.password, user.password)) {
          ctx.session.user = { id: user._id };
          return user;
        }
      }

      return null;
    },

    logout: (_, __, { ctx }) => {
      ctx.session.user = null;
      return true;
    }
  };
};

module.exports = sessionQueries;
