const ENV = (token, defVal = '') => process.env[token] || defVal;

const mailgun = {
  key: ENV('MAILGUN_API_KEY'),
  domain: ENV('MAILGUN_DOMAIN')
};

const config = {
  mailgun,
  DEFAULT_AVATAR: ENV('DEFAULT_AVATAR', '/anonymous-avatar.png'),
  GRAPHQL_PORT: 4000,
  MONGO_URL: ENV('MONGO_URL', 'mongodb://localhost/usbase')
};

module.exports = config;
