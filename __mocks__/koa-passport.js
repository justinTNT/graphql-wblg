const passport = jest.genMockFromModule('koa-passport');

let savedUser = Object.create(null);

passport.__setUser__ = (user) => {
  savedUser.id = user.id;
}

passport.authenticate = (strategy, args, cb) => {  // noop
  return cb;
}

module.exports = passport;

