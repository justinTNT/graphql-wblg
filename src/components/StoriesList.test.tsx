import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import StoriesList from './StoriesList';

describe('<StoriesList/>', () => {

  describe('smoke test', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(<StoriesList />);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<StoriesList />, div);
    });
  });

  describe('pagination', () => {
    //TODO
  });

});

