import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import SessionControl from './SessionControl';

describe('<SessionControl/>', () => {

  describe('smoke test', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(<SessionControl />);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<SessionControl />, div);
    });
  });

  describe('clicks', () => {
    // TODO
  });
});




