import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Tag from './Tag';

describe('<Tag/>', () => {

  describe('smoke test', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(<MemoryRouter><Tag /></MemoryRouter>);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<MemoryRouter><Tag /></MemoryRouter>, div);
    });
  });

});


