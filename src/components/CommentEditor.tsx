import React, { FunctionComponent, useEffect, useState } from 'react';
import { EditorState, ContentState, RichUtils } from 'draft-js';
import Editor from 'draft-js-plugins-editor';
import {
  ItalicButton,
  BoldButton,
  UnderlineButton,
  CodeButton
} from 'draft-js-buttons';

// @ts-ignore
import createToolbarPlugin from 'draft-js-static-toolbar-plugin';
import createToolbarLinkPlugin from 'draft-js-toolbar-link-plugin';

import { stateToHTML } from 'draft-js-export-html';
import { Button } from 'react-bootstrap';
import editorStyles from './CommentEditor.module.css';
import client from '../apollo';

import styles from './Comments.module.css';
import { ADD_COMMENT } from '../graph/mutations';
import { GetStory_story_comments } from '../graph/types/GetStory';
import { addCommentVariables } from '../graph/types/addComment';

const theme = {
  toolbarStyles: {
    toolbar: editorStyles.toolbar
  },
  buttonStyles: {
    button: editorStyles.button,
    buttonWrapper: editorStyles.buttonWrapper,
    active: editorStyles.buttonActive
  }
};

const toolbarLinkPlugin = createToolbarLinkPlugin({});
const { LinkButton } = toolbarLinkPlugin;

interface CommentEditorProps {
  parent: string;
  subject: string;
  doneEditing(newComment: GetStory_story_comments): void;
}

const CommentEditor: FunctionComponent<CommentEditorProps> = props => {
  const { parent, subject, doneEditing } = props;

  const [nothingToAdd, confirmNothingToAdd] = useState(true);
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const staticToolbarPlugin = createToolbarPlugin({ theme });
  const { Toolbar } = staticToolbarPlugin;

  const onChange = (edState: EditorState) => {
    const content: ContentState = edState.getCurrentContent();
    confirmNothingToAdd(!content.hasText() || !content.getPlainText().length);
    setEditorState(edState);
  };

  const handleKeyCommand = (command: string, edState: EditorState) => {
    const newState: EditorState | null = RichUtils.handleKeyCommand(
      edState,
      command
    );
    if (newState) {
      onChange(newState);
      return 'handled';
    }
    return 'not-handled';
  };

  const submitComment = () => {
    if (nothingToAdd) {
      return;
    }
    const comment: string = stateToHTML(editorState.getCurrentContent());
    const variables: addCommentVariables = { comment, parent, subject };
    const mutation = ADD_COMMENT;

    client.mutate({ mutation, variables }).then(({ data }) => {
      if (data) {
        const edState: EditorState = EditorState.push(
          editorState,
          ContentState.createFromText(''),
          'remove-range'
        );
        const newComment: GetStory_story_comments = (data as any).addComment;
        if (!newComment) {
          return;
        }

        doneEditing(newComment);
        setEditorState(edState);
      }
    });
  };

  let refsEditor: any;
  useEffect(() => {
    refsEditor.editor.focus();
  }, [refsEditor]);

  return (
    <div className={styles.commentCol}>
      {/*
      <Toolbar className={editorStyles.toolbar}>{
          // may be use React.Fragment instead of div to improve perfomance after React 16
        (externalProps:any) => (
          <div>
            <BoldButton {...externalProps} />
            <ItalicButton {...externalProps} />
            <UnderlineButton {...externalProps} />
            <CodeButton {...externalProps} />
          </div>
        )
      }</Toolbar>
    */}

      <Editor
        ref={(element: any) => {
          refsEditor = element;
        }}
        plugins={[staticToolbarPlugin, toolbarLinkPlugin]}
        placeholder="write a comment.."
        editorState={editorState}
        onChange={onChange}
        handleKeyCommand={handleKeyCommand}
      />

      <Button
        className={editorStyles.submitComment}
        variant="primary"
        onClick={submitComment}
        disabled={nothingToAdd}
      >
        <span className="fa fa-comment" />
        &nbsp; Add comment
      </Button>
    </div>
  );
};

export default CommentEditor;
