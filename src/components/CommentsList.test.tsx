import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import jest from 'jest-mock';
import CommentsList from './CommentsList';

describe('<CommentsList/>', () => {
  const scrollToSpy = jest.fn();
  global.scrollTo = scrollToSpy;

  describe('smoke test - no items', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(<CommentsList all={[]} />);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<CommentsList all={[]} />, div);
    });
  });

  describe('smoke test - one item', () => {
    const comment = {
      comment: 'test',
      id: '123'
    };

    it('renders (shallow) without error', () => {
      const app = shallow(<CommentsList all={[comment]} />);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<CommentsList all={[comment]} />, div);
    });
  });
});
