import React, { FunctionComponent, useState } from 'react';
import ld from 'lodash';

import client from '../apollo';

import styles from './Engagement.module.css';
import { GET_LOCAL_USER } from '../graph/client/queries';
import { ADD_SENTIMENT } from '../graph/mutations';

interface EngagementProps {
  likes: number;
  dislikes: number;
  sentiment: number;
  subject: string;
  className: string;
}

const Engagement: FunctionComponent<EngagementProps> = props => {
  const { likes, dislikes, sentiment, subject, className } = props;

  const [likesCount, setLikes] = useState(likes || 0);
  const [dislikesCount, setDislikes] = useState(dislikes || 0);
  const [sentimentFlag, setSentiment] = useState(sentiment || 0);
  const [hovering, setHovering] = useState(0);

  const submitSentiment = (
    newSentiment: number,
    newLikes: number,
    newDislikes: number
  ) => {
    const variables = { sentiment: newSentiment, subject };
    const mutation = ADD_SENTIMENT;

    client.mutate({ mutation, variables }).then(({ data }) => {
      if (data) {
        if ((data as any).addSentiment) {
          setSentiment(newSentiment);
          setLikes(newLikes);
          setDislikes(newDislikes);
        }
      }
    });
  };

  const confirmLoggedIn = async () => {
    const { data } = await client.query({ query: GET_LOCAL_USER });
    const active: boolean = !!(data && data.user);
    if (!active) {
      setSentiment(0);
    }
    return active;
  };

  const handleMouseEnter = (sent: number) => {
    return async () => {
      if (await confirmLoggedIn()) {
        setHovering(sent);
      }
    };
  };

  const handleMouseLeave = (sent: number) => {
    return () => {
      if (hovering === sent) {
        setHovering(0);
      }
    };
  };

  const handleClick = (sent: number) => {
    return async () => {
      let newLikes: number;
      let newDislikes: number;
      let newSentiment: number = sent;

      if (!(await confirmLoggedIn())) {
        return;
      }

      if (newSentiment < sentimentFlag) {
        newLikes = likesCount - sentimentFlag;
        newDislikes = dislikesCount + 1;
      } else if (newSentiment > sentimentFlag) {
        newLikes = likesCount + 1;
        newDislikes = dislikesCount + sentimentFlag;
      } else {
        newLikes = sentimentFlag > 0 ? likesCount - 1 : likesCount;
        newDislikes = sentimentFlag < 0 ? dislikesCount - 1 : dislikesCount;
        newSentiment = 0;
      }

      submitSentiment(newSentiment, newLikes, newDislikes);
    };
  };

  return (
    <div className={`${styles.engaged} ${className}`}>
      <div
        className={styles.dislikes}
        onMouseEnter={handleMouseEnter(-1)}
        onMouseLeave={handleMouseLeave(-1)}
        onClick={handleClick(-1)}
        role="presentation"
      >
        <span
          className={ld
            .compact([
              styles.engagementIcon,
              'fa fa-thumbs-down',
              hovering < 0 && styles.hoverDislike,
              sentimentFlag < 0 && styles.chosenDislike
            ])
            .join(' ')}
        />

        <span
          className={ld.compact([styles.count, styles.chosenDislike]).join(' ')}
        >
          {dislikesCount ? `- ${dislikesCount}` : <span>&nbsp;</span>}
        </span>
      </div>

      <div
        className={styles.likes}
        onMouseEnter={handleMouseEnter(1)}
        onMouseLeave={handleMouseLeave(1)}
        onClick={handleClick(1)}
        role="presentation"
      >
        <span
          className={ld
            .compact([
              styles.engagementIcon,
              'fa fa-thumbs-up',
              hovering > 0 && styles.hoverLike,
              sentimentFlag > 0 && styles.chosenLike
            ])
            .join(' ')}
        >
          {' '}
        </span>

        <span
          className={ld.compact([styles.count, styles.chosenLike]).join(' ')}
        >
          {likesCount ? `+ ${likesCount}` : <span>&nbsp;</span>}
        </span>
      </div>
    </div>
  );
};

export default Engagement;
