import ld from 'lodash';

import React from 'react';
import { Query } from 'react-apollo';

import styles from './StoryViewer.module.css';
import Tag from './Tag';
import Comments from './Comments';
import Engagement from './Engagement';
import { GET_STORY } from '../graph/queries';
import { GetStory_story_tags } from '../graph/types/GetStory';

interface Props {
  match: {
    params: {
      name: string;
    };
  };
}

export default (props: Props) => {
  const name: string = ld.get(props, 'match.params.name');
  return (
    <Query query={GET_STORY} variables={{ name }}>
      {({ loading, data }) => {
        return (
          !loading && (
            <div className="story">
              <div className="content">
                <h1 className={styles.title}>{data.story.title}</h1>
                <p dangerouslySetInnerHTML={{ __html: data.story.teaser }} />
              </div>

              <Engagement
                className={styles.engagement}
                likes={data.story.likes}
                dislikes={data.story.dislikes}
                sentiment={data.story.sentiment}
                subject={data.story.id}
              />

              <div className={styles.taglist}>
                {data.story.tags.map((tag: GetStory_story_tags) => (
                  <Tag name={tag.name} key={tag.id} />
                ))}
              </div>

              <Comments
                comments={data.story.comments}
                subject={data.story.id}
              />
            </div>
          )
        );
      }}
    </Query>
  );
};
