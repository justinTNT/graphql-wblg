import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import Engagement from './Engagement';

describe('<Engagement/>', () => {

  describe('smoke test', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(<Engagement />);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<Engagement />, div);
    });
  });

  describe('mouse events', () => {
    // TODO
  });
});



