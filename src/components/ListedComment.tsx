import React, { FunctionComponent, useState } from 'react';
import { Badge, Button, Image } from 'react-bootstrap';
import ld from 'lodash';
import styles from './Comments.module.css';
import CommentsList from './CommentsList';
import Engagement from './Engagement';
import { GetStory_story_comments } from '../graph/types/GetStory';

type Direction = 'Left' | 'Right';

interface ListedCommentProps {
  comment: GetStory_story_comments;
  direction: Direction;
  all: GetStory_story_comments[];
  subject: string;
}

const ListedComment: FunctionComponent<ListedCommentProps> = props => {
  const { comment, direction, all, subject } = props;

  const [hovering, hover] = useState(false);
  const [showingReplies, showReplies] = useState(false);
  const [showingEditor, showEditor] = useState(false);
  const [items, _] = useState(ld.filter(all, { parent: comment.id }));

  const handleMouseEnter = () => hover(true);
  const handleMouseLeave = () => hover(false);
  const hideEditor = () => showEditor(false);
  const revealReplies = () => {
    showReplies(true);
    showEditor(true);
  };

  const newDirection: Direction = direction === 'Left' ? 'Right' : 'Left';

  return (
    <div className="listedComment" key={comment.id}>
      <div className={styles.commentRow}>
        {direction === 'Left' && (
          <div className={styles.avatarCol}>
            <Image
              className={styles.avatar}
              src={comment.author.url}
              roundedCircle
            />
            <span className={styles.author}>{comment.author.name}</span>
          </div>
        )}

        <div
          className={styles.commentCol}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        >
          <div className={styles[`speechBubble${direction}`]}>
            <p dangerouslySetInnerHTML={{ __html: comment.comment }} />

            {hovering &&
              (items.length && !showingReplies ? (
                <span onClick={revealReplies} className={styles.repliesText}>
                  <Badge className={styles.commentsCounter}>
                    {items.length}
                  </Badge>
                  {items.length > 1 ? (
                    <span>replies...</span>
                  ) : (
                    <span>reply.</span>
                  )}
                </span>
              ) : (
                !showingEditor && (
                  <Button
                    className={styles.replyButton}
                    onClick={revealReplies}
                    variant="primary"
                  >
                    Reply
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span className="fa fa-comment" />
                  </Button>
                )
              ))}

            <Engagement
              className={styles.engagement}
              likes={comment.likes}
              dislikes={comment.dislikes}
              sentiment={comment.sentiment}
              subject={comment.id}
            />
          </div>
        </div>

        {direction === 'Right' && (
          <div className={styles.avatarCol}>
            <Image
              className={styles.avatar}
              src={comment.author.url}
              roundedCircle
            />
            <span className={styles.author}>{comment.author.name}</span>
          </div>
        )}
      </div>

      <div className={styles.commentRow}>
        {direction === 'Left' && <div className={styles.avatarCol} />}

        <div className={styles.commentCol}>
          {showingReplies && (
            <div className={styles.replies}>
              <CommentsList
                all={all}
                direction={newDirection}
                subject={subject}
                parent={comment.id}
                hideEditor={hideEditor}
              />
            </div>
          )}
        </div>

        {direction === 'Right' && <div className={styles.avatarCol} />}
      </div>
    </div>
  );
};

export default ListedComment;
