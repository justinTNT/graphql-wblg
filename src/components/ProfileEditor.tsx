import React, { FunctionComponent, useState, useEffect } from 'react';
import { Modal, Button, FormControl, InputGroup } from 'react-bootstrap';
import AvatarEditor from 'react-avatar-editor';

import config from '../config';
import client from '../apollo';
import { GET_CURRENT_USER } from '../graph/queries';
import { GetCurrentUser } from '../graph/types/GetCurrentUser';
import { UPDATE_PROFILE } from '../graph/mutations';
import { updateProfileVariables } from '../graph/types/updateProfile';
import styles from './ProfileEditor.module.css';

interface ProfileProps {
  editProfile: (flag: boolean) => void;
}

const ProfileEditor: FunctionComponent<ProfileProps> = props => {
  const { editProfile } = props;

  let editor!: AvatarEditor;

  const [url, setURL] = useState(config.DEFAULT_AVATAR);
  const [handle, setHandle] = useState('unverified user');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [slider, setSlider] = useState(1.0);
  const [imgChangeCnt, setImgChangeCnt] = useState(0);
  const [image, setImageFile] = useState(null);

  useEffect(() => {
    (async () => {
      const { data }: { data: GetCurrentUser } = await client.query({
        query: GET_CURRENT_USER
      });
      /* eslint-disable no-shadow */
      if (data.getCurrentUser) {
        const { handle, url, name, email } = data.getCurrentUser;
        setHandle(handle);
        setURL(url);
        setName(name);
        setEmail(email);
      }
      /* eslint-enable no-shadow */
    })();
  }, []);

  const handleHide = () => {
    editProfile(false);
  };

  const setEdRef = (ed: AvatarEditor) => {
    if (ed) {
      editor = ed;
    }
  };

  const handleSave = () => {
    const variables: updateProfileVariables = { name, email };
    if (imgChangeCnt > 1) {
      // we change once when we assign url after mount
      variables.img = editor.getImageScaledToCanvas().toDataURL();
    }
    const mutation = UPDATE_PROFILE;

    client.mutate({ mutation, variables }).then(({ data }) => {
      if (data) {
        const newProfile = (data as any).updateProfile;
        if (newProfile) {
          editProfile(false);
        }
      }
    });
  };

  const handleScale = (e: any) => {
    setSlider(parseFloat(e.target.value));
  };

  const handleNewImage = (e: any) => {
    setImageFile(e.target.files[0]);
  };

  const handleChangeEmail = (event: any) => {
    setEmail((event.target as HTMLInputElement).value);
  };

  const handleChangeDisplayName = (event: any) => {
    setName((event.target as HTMLInputElement).value);
  };

  const handleChangeImage = () => {
    setImgChangeCnt(imgChangeCnt + 1);
  };

  return (
    <Modal show onHide={handleHide}>
      <Modal.Header closeButton>
        <Modal.Title>{handle}</Modal.Title>
      </Modal.Header>

      <Modal.Body className={styles.modalBody}>
        <AvatarEditor
          className={styles.profileEditor}
          ref={setEdRef}
          image={image || url}
          width={250}
          height={250}
          border={50}
          borderRadius={125}
          color={[255, 255, 255, 0.6]} // RGBA
          crossOrigin="anonymous"
          scale={slider}
          rotate={0}
          onImageChange={handleChangeImage}
        />
        <input
          name="scale"
          type="range"
          onChange={handleScale}
          min="0.4"
          max="1.8"
          step="0.05"
          defaultValue="1"
        />
        <input name="newImage" type="file" onChange={handleNewImage} />
        <hr />
        <br />

        <InputGroup>
          <InputGroup.Prepend className={styles.prepend}>
            <InputGroup.Text>Display name</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="text"
            placeholder="display name"
            value={name}
            onChange={handleChangeDisplayName as any}
          />
        </InputGroup>

        <InputGroup>
          <InputGroup.Prepend className={styles.prepend}>
            <InputGroup.Text>Email address</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="text"
            placeholder="email address"
            value={email}
            onChange={handleChangeEmail as any}
          />
        </InputGroup>
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={handleHide}>Close</Button>
        <Button onClick={handleSave}>Save</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ProfileEditor;
