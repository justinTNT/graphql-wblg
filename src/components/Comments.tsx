import React, { FunctionComponent, useState } from 'react';

import { Badge, Card } from 'react-bootstrap';

import CommentsList from './CommentsList';
import styles from './Comments.module.css';
import { GetStory_story_comments } from '../graph/types/GetStory';

interface CommentsProps {
  comments: GetStory_story_comments[];
  subject: string;
}

const Comments: FunctionComponent<CommentsProps> = ({ comments, subject }) => {
  const [isOpen, open] = useState(false);
  const [hovering, hover] = useState(false);

  const togglePanel = () => {
    hover(false);
    open(!isOpen);
  };
  const handleMouseLeave = () => hover(false);
  const handleMouseEnter = () => hover(true);

  return (
    <Card
      className="comments"
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <Card.Title className={styles.commentsTitle} onClick={togglePanel}>
        <span>
          {' '}
          {comments.length > 0 && (
            <Badge className={styles.commentsCounter}>
              {' '}
              {comments.length}{' '}
            </Badge>
          )}{' '}
          Comments &nbsp;
        </span>
        {hovering &&
          (isOpen ? (
            <span className="icon fa fa-chevron-up" />
          ) : (
            <span className="icon fa fa-chevron-down" />
          ))}
      </Card.Title>

      <Card.Body>
        {isOpen && (
          <CommentsList
            direction="Left"
            all={comments}
            subject={subject}
            parent={subject}
          />
        )}
      </Card.Body>
    </Card>
  );
};

export default Comments;
