import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import { Editor, EditorState, ContentState, RichUtils } from 'draft-js';

import ld from 'lodash';

import { stateToHTML } from 'draft-js-export-html';
import { Button, Image } from 'react-bootstrap';
import client from '../apollo';
import config from '../config';

import ListedComment from './ListedComment';
import CommentEditor from './CommentEditor';
import styles from './Comments.module.css';
import { ADD_COMMENT } from '../graph/mutations';
import { GET_CURRENT_USER } from '../graph/queries';
import { GetStory_story_comments } from '../graph/types/GetStory';
import { addCommentVariables } from '../graph/types/addComment';
import { GetCurrentUser } from '../graph/types/GetCurrentUser';

type Direction = 'Left' | 'Right';

interface CommentsListProps {
  parent: string;
  subject: string;
  direction: Direction;
  all: GetStory_story_comments[];
  hideEditor?(): void;
}

const CommentsList: FunctionComponent<CommentsListProps> = props => {
  const { parent, subject, direction, all, hideEditor } = props;

  const [items, setItems] = useState(ld.filter(all, { parent }));
  const [showingEditor, changeShowingEditor] = useState(true);

  const [url, setURL] = useState(config.DEFAULT_AVATAR);
  const [name, setName] = useState('');

  useEffect(() => {
    (async () => {
      const { data }: { data: GetCurrentUser } = await client.query({
        query: GET_CURRENT_USER
      });
      /* eslint-disable no-shadow */
      if (data.getCurrentUser) {
        const { url, name } = data.getCurrentUser;
        setURL(url);
        setName(name);
      }
      /* eslint-enable no-shadow */
    })();
  }, []);

  const doneEditing = (newComment: GetStory_story_comments) => {
    if (hideEditor) {
      hideEditor();
      changeShowingEditor(false);
    }
    const newItems: GetStory_story_comments[] = [...items, newComment];
    setItems(newItems);
  };

  return (
    <div className="CommentsList">
      <div className={styles.commentsList}>
        {items.map(c => {
          return (
            <ListedComment
              direction={direction}
              all={all}
              comment={c}
              subject={subject}
              key={c.id}
            />
          );
        })}
      </div>

      {showingEditor && (
        <div className={`${styles.newComment} ${styles.commentRow}`}>
          {direction === 'Left' && (
            <div className={styles.avatarCol}>
              <Image className={styles.avatar} src={url} roundedCircle />
              <span className={styles.author}>{name}</span>
            </div>
          )}

          <CommentEditor
            parent={parent}
            subject={subject}
            doneEditing={doneEditing}
          />

          {direction === 'Right' && (
            <div className={styles.avatarCol}>
              <Image className={styles.avatar} src={url} roundedCircle />
              <span className={styles.author}>{name}</span>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default CommentsList;
