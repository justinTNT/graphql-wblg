import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import StoryViewer from './StoryViewer';
import { MockedProvider } from 'react-apollo/test-utils';

import { GET_STORY } from '../graph/queries';

const mocks = [
  {
    request: {
      query: GET_STORY,
      variables: {
        name: 'test',
      },
    },
    result: {
      data: {
        story: { id: '105', name: 'test' },
      },
    },
  },
];


describe('<StoryViewer/>', () => {

  describe('smoke test', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(
        <MockedProvider mocks={mocks} addTypename={false}>
          <StoryViewer match={{params: { name: 'test' } }} />
        </MockedProvider>
      );
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(
        <MockedProvider mocks={mocks} addTypename={false}>
          <StoryViewer match={{params: { name: 'test' } }} />
        </MockedProvider>
        , div);
    });
  });
});

