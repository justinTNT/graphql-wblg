import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Tag.module.css';

interface Props {
  name: string;
}

export default ({ name }: Props) => (
  <Link to={`/tag/${name}`}>
    <span className={styles.tag}> {name} </span>
  </Link>
);
