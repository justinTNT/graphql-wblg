import React, { FunctionComponent, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import ld from 'lodash';

import client from '../apollo';

import { GET_STORIES } from '../graph/queries';
import {
  GetStories_stories,
  GetStoriesVariables
} from '../graph/types/GetStories';

const StoryLister: FunctionComponent<RouteComponentProps> = props => {
  const tag: string = ld.get(props, 'match.params.tag');
  const initItems: GetStories_stories[] = [];
  const [items, setItems] = useState(initItems);
  const [isFull, setFull] = useState(false);

  const appendMoreStories = async (skip: number) => {
    const variables: GetStoriesVariables = { tag, skip };
    try {
      const data = await client.query({ query: GET_STORIES, variables });
      const newItems: GetStories_stories[] = ld.get(data, 'data.stories') || [];
      if (newItems.length) {
        setItems(previous => previous.concat(newItems));
      } else {
        setFull(true);
      }
    } catch (err) {
      setFull(true);
    }
  };

  const onScroll = (skip: number) => () => {
    if (
      window.innerHeight + window.scrollY >=
      document.body.offsetHeight - 500
    ) {
      appendMoreStories(skip);
    }
  };

  if (items.length === 0) {
    appendMoreStories(0);
  } // async init

  useEffect(() => {
    const listener = onScroll(items.length);
    if (!isFull) {
      window.addEventListener('scroll', listener, false);
    }
    return () => {
      window.removeEventListener('scroll', listener, false);
    };
  }, [items.length, isFull, onScroll]);

  return (
    <div>
      {items.map((story: GetStories_stories) => (
        <div className="listedStory story" key={story.id}>
          <h1 className="title">
            <Link to={`/${story.name}`}>{story.title}</Link>
          </h1>
          <p dangerouslySetInnerHTML={{ __html: story.teaser }} />
        </div>
      ))}
    </div>
  );
};

export default StoryLister;
