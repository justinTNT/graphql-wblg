import React, { FunctionComponent, useState, useEffect } from 'react';
import {
  Image,
  Tooltip,
  Modal,
  Button,
  Col,
  Form,
  FormControl,
  FormGroup,
  InputGroup
} from 'react-bootstrap';
import ld from 'lodash';
import { GraphQLError } from 'graphql';
import FacebookLogin from 'react-facebook-login';

import config from '../config';
import client from '../apollo';
import styles from './SessionControl.module.css';
import { GET_CURRENT_USER } from '../graph/queries';
import {
  POST_REGISTER,
  PUT_FACEBOOK,
  PUT_LOGIN,
  PUT_LOGOUT
} from '../graph/mutations';

import { Register, RegisterVariables } from '../graph/types/Register';
import { Login, LoginVariables } from '../graph/types/Login';
import { Facebook, FacebookVariables } from '../graph/types/Facebook';
import { Logout } from '../graph/types/Logout';
import { GetCurrentUser } from '../graph/types/GetCurrentUser';

interface SessionProps {
  editProfile: (flag: boolean) => void;
}

const SessionControl: FunctionComponent<SessionProps> = props => {
  const { editProfile } = props;

  const [loggedIn, logIn] = useState(false);
  const [hovering, hover] = useState(false);
  const [registering, switchToRegister] = useState(false);
  const [showing, show] = useState(false);
  const [failing, setFailed] = useState(false);
  const [avatar, setURL] = useState(config.DEFAULT_AVATAR);
  const [pass, setPass] = useState('');
  const [user, setUser] = useState('');
  const [email, setEmail] = useState('');
  const [errorMsg, setErrorMsg] = useState('');

  const setUserLoggedIn = (id: string, newUrl?: string) => {
    show(false);
    logIn(!!id);
    setURL(!!id && newUrl ? newUrl : config.DEFAULT_AVATAR);
    client.writeData({ data: { user: id } });
  };

  useEffect(() => {
    (async () => {
      const { data }: { data: GetCurrentUser } = await client.query({
        query: GET_CURRENT_USER
      });
      const id = ld.get(data, 'getCurrentUser.id') || '';
      if (id) {
        setURL(ld.get(data, 'getCurrentUser.url'));
      }
      logIn(id);
    })();
  }, []);

  const submitOnEnter = (cont: () => void) => ({ key }: { key: string }) =>
    key === 'Enter' && cont();

  const register = async () => {
    if (!registering) {
      return;
    }

    const variables: RegisterVariables = { user, pass, email };

    try {
      const result: any = await client.mutate({
        mutation: POST_REGISTER,
        variables
      });
      if (result) {
        const { data }: { data: Register } = result;
        if (data.register) {
          const { id, url } = data.register;
          setUserLoggedIn(id, url);
        }
      }
    } catch (err) {
      if (Array.isArray(err.graphQLErrors)) {
        err.graphQLErrors.forEach((graphQLError: GraphQLError) => {
          if (graphQLError.message) {
            setErrorMsg(graphQLError.message);
          }
        });
      }
    }

    // fall thru to failure
    setFailed(true);
  };

  const login = async () => {
    if (registering) {
      return;
    }

    const variables: LoginVariables = { user, pass };

    try {
      const result: any = await client.mutate({
        mutation: PUT_LOGIN,
        variables
      });
      if (result) {
        const { data }: { data: Login } = result;
        if (data.login) {
          const { id, url } = data.login;
          setUserLoggedIn(id, url);
        }
      }
    } catch (err) {
      setFailed(true);
    }

    // fall thru to failure
    setFailed(true);
  };

  const loginOrRegister = () => {
    setFailed(false);
    setErrorMsg('');
    if (registering) {
      register();
    } else {
      login();
    }
  };

  const logout = async () => {
    const result: any = await client.mutate({ mutation: PUT_LOGOUT });

    if (result) {
      const { data }: { data: Logout } = result;
      if (data.logout) {
        client.resetStore();
        setUserLoggedIn('');
      }
    }
  };

  const maybeEditProfile = () => {
    if (loggedIn) {
      editProfile(true);
    }
  };
  const toggleRegister = () => {
    setFailed(false);
    switchToRegister(!registering);
  };
  const handleShow = () => {
    setFailed(false);
    show(true);
  };
  const handleHide = () => {
    setFailed(false);
    show(false);
  };
  const handleMouseEnter = () => {
    hover(true);
  };
  const handleMouseLeave = () => {
    hover(false);
  };
  const handleChangeUsername = (event: any) => {
    setUser((event.target as HTMLInputElement).value);
  };
  const handleChangePassword = (event: any) => {
    setPass((event.target as HTMLInputElement).value);
  };
  const handleChangeEmail = (event: any) => {
    setEmail((event.target as HTMLInputElement).value);
  };
  const handleClick = () => {
    hover(false);
    if (loggedIn) {
      logout();
    } else {
      show(true);
    }
  };

  const responseFacebook = async (fbData: any) => {
    const variables: FacebookVariables = {
      user: fbData.name,
      email: fbData.email,
      url: ld.get(fbData, 'picture.data.url')
    };

    const result: any = await client.mutate({
      mutation: PUT_FACEBOOK,
      variables
    });
    if (result) {
      const { data }: { data: Facebook } = result;
      if (data.facebook) {
        setUserLoggedIn(data.facebook.id, data.facebook.url);
      }
    }
  };

  return (
    <div className={styles.session}>
      <Image
        className={`${styles.avatar} ${(!loggedIn && styles.notLoggedIn) ||
          ''}`}
        src={avatar}
        roundedCircle
        onClick={maybeEditProfile}
      />
      <span
        className={[
          styles[`hover${hovering ? 'On' : 'Off'}`],
          'fa',
          `fa-sign-${loggedIn ? 'out' : 'in'}`
        ].join(' ')}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={handleClick}
      >
        {hovering && (
          <Tooltip placement="bottom" className="in" id="tooltip-bottom">
            {loggedIn ? 'Logout' : 'Login'}
          </Tooltip>
        )}
      </span>

      <Modal show={showing} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>{registering ? 'Register' : 'Log In'}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form>
            <FormGroup controlId="formHorizontalEmail">
              <Col>
                <InputGroup>
                  <InputGroup.Prepend className={styles.prepend}>
                    <InputGroup.Text>User name</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    type="text"
                    placeholder="Username"
                    value={user}
                    className={errorMsg === 'username' ? styles.error : ''}
                    onChange={handleChangeUsername as any}
                  />
                </InputGroup>
              </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalPassword">
              <Col>
                <InputGroup>
                  <InputGroup.Prepend className={styles.prepend}>
                    <InputGroup.Text>Password</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    type="password"
                    placeholder="Password"
                    value={pass}
                    onKeyPress={submitOnEnter(login)}
                    onChange={handleChangePassword as any}
                  />
                </InputGroup>
              </Col>
            </FormGroup>

            {registering && (
              <FormGroup controlId="formHorizontalEmail">
                <Col>
                  <InputGroup>
                    <InputGroup.Prepend className={styles.prepend}>
                      <InputGroup.Text>Email</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      type="email"
                      placeholder="Email address"
                      value={email}
                      className={errorMsg === 'email' ? styles.error : ''}
                      onKeyPress={submitOnEnter(register)}
                      onChange={handleChangeEmail as any}
                    />
                  </InputGroup>
                </Col>
              </FormGroup>
            )}

            {failing && (
              <div className={styles.invalid}>
                {registering
                  ? `${errorMsg} already exists`
                  : 'Invalid credentials'}
              </div>
            )}

            <FormGroup className={styles.facebook}>
              <hr />
              or
              <br />
              <FacebookLogin
                fields="name,email,picture"
                appId={config.facebook.APP_ID}
                autoLoad
                callback={responseFacebook}
                icon="fa-facebook"
              />
            </FormGroup>
          </Form>
        </Modal.Body>

        <Modal.Footer>
          <div className={styles.lefty}>
            <i>
              {registering ? 'Already registered?' : 'New user?'}
              &nbsp;
              <a className={styles.link} onClick={toggleRegister}>
                {registering ? 'login' : 'register'}
              </a>
              .
            </i>
          </div>
          <Button variant="info" onClick={handleHide}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={loginOrRegister}
            disabled={pass.length < 8}
          >
            {registering ? 'Register' : 'Login'}
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default SessionControl;
