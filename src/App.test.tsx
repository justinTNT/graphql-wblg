import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import App from './App';

describe('<App/>', () => {

  describe('smoke test', () => {
    it('renders (shallow) without error', () => {
      const app = shallow(<App />);
      expect(app).toHaveLength(1);
    });

    it('renders (deep) without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<App />, div);
    });
  });

});
