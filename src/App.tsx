import React, { FunctionComponent, useState } from 'react';
import { Route, Link, BrowserRouter, Switch } from 'react-router-dom';

import StoryViewer from './components/StoryViewer';
import StoriesList from './components/StoriesList';
import SessionControl from './components/SessionControl';
import ProfileEditor from './components/ProfileEditor';

import config from './config';
import './App.css';

const App: FunctionComponent<{}> = () => {
  const [editingProfile, editProfile] = useState(false);

  return (
    <BrowserRouter>
      <div>
        <div id="toplogo">
          <SessionControl editProfile={editProfile} />
          {editingProfile && <ProfileEditor editProfile={editProfile} />}
          <p>{config.BLOG_NAME}</p>
          <Link to="/">
            <img alt="blog" id="bloglog" src={config.LOGO_URL} />
          </Link>
        </div>

        <main>
          <Switch>
            <Route exact path="/" component={StoriesList} />
            <Route path="/tag/:tag" component={StoriesList} />
            <Route path="/:name" component={StoryViewer} />
          </Switch>
        </main>
      </div>
    </BrowserRouter>
  );
};

export default App;
