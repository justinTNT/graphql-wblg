import gql from 'graphql-tag';

const ADD_COMMENT = gql`
  mutation addComment($comment: String, $subject: ID, $parent: ID) {
    addComment(comment: $comment, subject: $subject, parent: $parent) {
      id
      author {
        name
        url
      }
      comment
    }
  }
`;

const ADD_SENTIMENT = gql`
  mutation addSentiment($sentiment: Int, $subject: ID) {
    addSentiment(sentiment: $sentiment, subject: $subject)
  }
`;

const UPDATE_PROFILE = gql`
  mutation updateProfile($img: String, $name: String!, $email: String!) {
    updateProfile(img: $img, name: $name, email: $email) {
      url
    }
  }
`;

const POST_REGISTER = gql`
  mutation Register($user: String!, $pass: String!, $email: String!) {
    register(username: $user, password: $pass, email: $email) {
      id
      url
    }
  }
`;

const PUT_FACEBOOK = gql`
  mutation Facebook($user: String!, $url: String, $email: String) {
    facebook(user: $user, url: $url, email: $email) {
      id
      url
    }
  }
`;

const PUT_LOGIN = gql`
  mutation Login($user: String!, $pass: String!) {
    login(username: $user, password: $pass) {
      id
      url
    }
  }
`;

const PUT_LOGOUT = gql`
  mutation Logout {
    logout
  }
`;

export { ADD_COMMENT, ADD_SENTIMENT, UPDATE_PROFILE, POST_REGISTER, PUT_FACEBOOK, PUT_LOGIN, PUT_LOGOUT };

