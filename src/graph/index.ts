import * as query from './queries';
import * as mutation from './mutations';

export default { query, mutation };
