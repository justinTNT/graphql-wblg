import gql from 'graphql-tag';

const SET_LOCAL_USER = gql`
  mutation setLocalUser($id: ID) {
    setLocalUser(id: $id) {
      id
    }
  }
`;

export { SET_LOCAL_USER };
