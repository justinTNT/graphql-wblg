// import gql from 'graphql-tag';

const defaults = { user: '' };

const resolvers = {};

const typeDefs = `
  type Query {
    user: String
  }
`;

export default { defaults, resolvers, typeDefs };
