import gql from 'graphql-tag';

const GET_LOCAL_USER = gql`
  {
    user @client
  }
`;

export { GET_LOCAL_USER };
