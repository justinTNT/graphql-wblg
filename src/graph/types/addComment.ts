/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: addComment
// ====================================================

export interface addComment_addComment_author {
  __typename: "Guest";
  name: string;
  url: string | null;
}

export interface addComment_addComment {
  __typename: "Comment";
  id: string;
  author: addComment_addComment_author;
  comment: string;
}

export interface addComment {
  addComment: addComment_addComment | null;
}

export interface addCommentVariables {
  comment?: string | null;
  subject?: string | null;
  parent?: string | null;
}
