/* tslint:disable */

// ====================================================
// GraphQL query operation: Register
// ====================================================

export interface Register_register {
  __typename: "Guest";
  id: string;
  name: string;
  url: string;
}

export interface Register {
  register: Register_register | null;
}

export interface RegisterVariables {
  user: string;
  pass: string;
  email: string;
}
