/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetStories
// ====================================================

export interface GetStories_stories {
  __typename: "Story";
  id: string;
  name: string;
  title: string;
  teaser: string;
}

export interface GetStories {
  stories: (GetStories_stories | null)[] | null;
}

export interface GetStoriesVariables {
  skip?: number | null;
  tag?: string | null;
}
