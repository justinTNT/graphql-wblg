/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Logout
// ====================================================

export interface Logout {
  logout: boolean | null;
}
