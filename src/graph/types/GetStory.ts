/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetStory
// ====================================================

export interface GetStory_story_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface GetStory_story_comments_author {
  __typename: "Guest";
  id: string;
  name: string;
  url: string;
}

export interface GetStory_story_comments {
  __typename: "Comment";
  id: string;
  author: GetStory_story_comments_author;
  comment: string;
  likes: number;
  dislikes: number;
  sentiment: number;
  parent: string;
}

export interface GetStory_story {
  __typename: "Story";
  id: string;
  name: string;
  title: string;
  teaser: string;
  tags: (GetStory_story_tags | null)[] | null;
  sentiment: number | null;
  likes: number | null;
  dislikes: number | null;
  comments: (GetStory_story_comments | null)[] | null;
}

export interface GetStory {
  story: GetStory_story | null;
}

export interface GetStoryVariables {
  name: string;
}
