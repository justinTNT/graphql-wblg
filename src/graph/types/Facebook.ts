/* tslint:disable */

// ====================================================
// GraphQL query operation: Facebook
// ====================================================

export interface Facebook_facebook {
  __typename: "Guest";
  id: string;
  name: string;
  url: string;
}

export interface Facebook {
  facebook: Facebook_facebook | null;
}

export interface FacebookVariables {
  user: string;
  url: string;
  email: string;
}
