export interface updateProfileVariables {
  img?: string;
  name: string;
  email: string;
}
