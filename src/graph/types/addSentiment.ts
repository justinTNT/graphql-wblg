/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: addSentiment
// ====================================================

export interface addSentiment {
  addSentiment: boolean | null;
}

export interface addSentimentVariables {
  sentiment?: number | null;
  subject?: string | null;
}
