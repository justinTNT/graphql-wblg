/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Login
// ====================================================

export interface Login_login {
  __typename: "Guest";
  id: string;
  name: string;
  url: string;
}

export interface Login {
  login: Login_login | null;
}

export interface LoginVariables {
  user: string;
  pass: string;
}
