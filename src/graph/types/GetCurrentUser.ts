/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetCurrentUser
// ====================================================

export interface GetCurrentUser_getCurrentUser {
  __typename: "Guest";
  id: string;
  handle: string;
  name: string;
  email: string;
  url: string;
}

export interface GetCurrentUser {
  getCurrentUser: GetCurrentUser_getCurrentUser | null;
}
