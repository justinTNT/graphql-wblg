import gql from 'graphql-tag';

const GET_CURRENT_USER = gql`
  query GetCurrentUser {
    getCurrentUser {
      id
      handle
      name
      email
      url
    }
  }
`;

const GET_STORIES = gql`
  query GetStories($skip: Int, $tag: String) {
    stories(first:10, skip:$skip, tag:$tag) {
      id
      name
      title
      teaser
    }
  }
`;

const GET_STORY = gql`
  query GetStory($name: String!) {
    story(name: $name) {
      id
      name
      title
      teaser
      tags { 
        id
        name
      }
      sentiment
      likes
      dislikes
      comments {
        id
        author {
          id
          name
          url
        }
        comment
        likes
        dislikes
        sentiment
        parent
      }
    }
  }
`;

export { GET_CURRENT_USER, GET_STORY, GET_STORIES };
