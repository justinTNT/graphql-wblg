const ENV = (token, defVal = '') => process.env[`REACT_APP_${token}`] || defVal;

const facebook = {
  APP_ID: ENV('FACEBOOK_ID')
};

const config = {
  facebook,
  BLOG_NAME: ENV('BLOG_NAME', 'MT Muse'),
  LOGO_URL: ENV('LOGO_URL'),
  GRAPH_URI: ENV('GRAPH_URI'),
  DEFAULT_AVATAR: ENV('DEFAULT_AVATAR', 'anonymous-avatar.png')
};

export default config;
