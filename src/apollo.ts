import ApolloClient from 'apollo-boost';
import config from './config';
import clientState from './graph/client';

const uri = config.GRAPH_URI;
const credentials = 'include';
export default new ApolloClient({ uri, credentials, clientState });
